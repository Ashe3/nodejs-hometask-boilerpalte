const isEmpty = require('lodash.isempty');

const { UserRepository } = require('../repositories/userRepository');

class UserService {

  createUser(userData) {
    let user;
    try {
      user = UserRepository.create(userData);
    }
    catch { }
    finally {
      if (!user) {
        return null;
      }
      return user;
    }
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getUsersList() {
    const usersList = UserRepository.getAll();
    if (!usersList) {
      return null;
    }
    return usersList;
  }

  getUser(id) {
    const user = this.search(id);
    if (!user) {
      return null
    }
    return user;
  }

  removeUser(id) {
    const removedUser = UserRepository.delete(id);
    if (isEmpty(removedUser)) {
      return null;
    }
    return removedUser;
  }

  updateUser(id, data) {
    const { email } = data;

    if (email) {
      const emailSearch = this.getUser({ email });

      if (emailSearch && (emailSearch.id !== id)) {
        return null
      }
    }

    const updatedUser = UserRepository.update(id, data);
    return updatedUser;
  }

}

module.exports = new UserService();