const isEmpty = require('lodash.isempty');

const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

  getFilghterList() {
    const fightersList = FighterRepository.getAll();
    if (!fightersList) {
      return null;
    }
    return fightersList;
  }

  createFighter(data) {
    let newFighter;
    try {
      newFighter = FighterRepository.create(data);
    } catch { }
    finally {
      if (!newFighter) {
        return null;
      }
      return newFighter;
    }
  }

  search(data) {
    const fighterData = FighterRepository.getOne(data);
    if (!fighterData) {
      return null;
    }
    return fighterData;
  }

  removeFighter(id) {
    const removedFighter = FighterRepository.delete(id);
    if (isEmpty(removedFighter)) {
      return null;
    }
    return removedFighter;
  }

  updateFighter(id, data) {
    const fighter = this.search({ id });
    const { name } = data;

    if (!fighter) {
      return null;
    }

    if (name) {
      const existByName = this.search({ name });
      if (existByName && existByName.id !== id) {
        return null;
      }
    }

    const updatedFighter = FighterRepository.update(id, data);
    return updatedFighter;
  }
}

module.exports = new FighterService();