const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', createUserValid, (request, response, next) => {
  const { email, phoneNumber } = request.body;
  const { body: user } = request;

  if (UserService.search({ email })) {
    response.err = {
      message: 'User alredy exist!'
    }
  } else if (UserService.search({ phoneNumber })) {
    response.err = {
      message: 'Phone number is alredy in use!'
    }
  } else {
    const userInfo = UserService.createUser(user);

    if (!userInfo) {
      response.err = {
        message: 'Failed to create user'
      };
    } else {
      response.data = user;
    }
  }
  next();

}, responseMiddleware);

router.get('/', (request, response, next) => {
  const usersListInfo = UserService.getUsersList();

  if (!usersListInfo) {
    response.err = {
      message: 'Failed to get users list'
    };
  } else {
    response.data = usersListInfo;
  }
  next();

}, responseMiddleware);

router.get('/:id', (request, response, next) => {
  const { id } = request.params;
  const userInfo = UserService.getUser({ id });

  if (!userInfo) {
    response.err = {
      message: 'User not found!'
    }
  } else {
    response.data = userInfo;
  }
  next();

}, responseMiddleware);

router.put('/:id', updateUserValid, (request, response, next) => {
  const { id } = request.params;
  const userInfo = UserService.getUser({ id });
  const dataToUpdate = request.body;

  if (!userInfo) {
    response.err = {
      message: 'User not found!'
    };
  } else {
    const userInfoUpdate = UserService.updateUser(id, dataToUpdate);
    if (!userInfoUpdate) {
      response.err = {
        message: `Failed to update user id:${id}`
      };
    } else {
      response.data = userInfoUpdate;
    }
  }
  next();

}, responseMiddleware);

router.delete('/:id', (request, response, next) => {
  const { id } = request.params;
  const removedUserinfo = UserService.removeUser(id);

  if (!removedUserinfo) {
    response.err = {
      status: 404,
      message: `Failed to delete user id:${id}`
    };
  } else {
    response.data = {
      message: `User id:${id} successfully deleted`
    }
  }
  next();
}, responseMiddleware);

module.exports = router;