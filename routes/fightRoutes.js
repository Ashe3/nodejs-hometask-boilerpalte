const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

router.get('/', (req, res) => {
  next();
}, responseMiddleware);

router.get('/:id', (req, res) => {
  next();
}, responseMiddleware);

router.post('/', (req, res) => {
  next();
}, responseMiddleware);

router.put('/:id', (req, res) => {
  next();
}, responseMiddleware);

router.delete('/:id', (req, res) => {
  next();
}, responseMiddleware);

module.exports = router;