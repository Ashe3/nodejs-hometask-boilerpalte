const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {

  const fightersList = FighterService.getFilghterList();
  if (!fightersList) {
    res.err = {
      message: 'Failed to get fightres list'
    };
  } else {
    res.data = fightersList;
  }
  next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {

  const { id } = req.params;
  const fighterInfo = FighterService.search({ id });
  if (!fighterInfo) {
    res.err = {
      status: 404,
      message: 'Fighter not found!'
    }
  } else {
    res.data = fighterInfo;
  }
  next();
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {

  const { body: fighterInfo } = req;
  const { name } = fighterInfo;
  if (FighterService.search({ name })) {
    res.err = {
      message: 'Fighter already exist!'
    }
  } else {
    const newFighter = FighterService.createFighter(fighterInfo);
    if (!newFighter) {
      req.err = {
        message: 'Failed to create new fighter!'
      }
    } else {
      res.data = newFighter;
    }
  }
  next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {

  const { id } = req.params;
  const updatedFighter = FighterService.updateFighter(id, req.body);
  if (!updatedFighter) {
    res.err = {
      message: `Failed to update fighter id:${id}`
    };
  } else {
    res.data = updatedFighter;
  }
  next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  const { id } = req.params;
  const removedFighterinfo = FighterService.removeFighter(id);

  if (!removedFighterinfo) {
    res.err = {
      status: 404,
      message: `Failed to delete fighter id:${id}`
    };
  } else {
    res.data = {
      message: `Fighter id:${id} successfully deleted`
    }
  }
  next();
}, responseMiddleware);

module.exports = router;