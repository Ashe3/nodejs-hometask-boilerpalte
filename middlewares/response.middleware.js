const responseMiddleware = (req, res, next) => {

  if (res.err) {
    const { message, status } = res.err;

    res.status(status || 400).json({
      error: 'true',
      message
    });
  } else {
    const { data: serverResponse } = res;
    res.status(200).json(serverResponse);
  }
  next();

}

exports.responseMiddleware = responseMiddleware;