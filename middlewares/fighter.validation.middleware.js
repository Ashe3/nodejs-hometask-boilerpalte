const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {

  const { name, power, defense, id } = req.body;
  const isDataComplete = [name, power, defense].every(value => !!value);

  if (id) {
    res.status(400).json({
      error: 'true',
      message: 'Failed to confirm fighter data'
    });
  } else if (!isDataComplete) {
    res.status(400).json({
      error: 'true',
      message: 'Fighter data is not complete'
    });
  } else if (isDataValid({ isName: true, isPower: true, isDefense: true }, req.body, res)) {
    req.body = { name, power, defense, health: 100 };
    next();
  }
}

const updateFighterValid = (req, res, next) => {

  const { name, power, defense } = req.body;

  if (isDataValid({ isName: !!name, isPower: !!power, isDefense: !!defense }, req.body, res)) {
    req.body = { name, power, defense, health: 100 };
    Object.keys(req.body).forEach(key => !req.body[key] && delete req.body[key]);
    next()
  }

}

function isDataValid({ isName = false, isPower = false, isDefense = false }, fighterData, response) {

  const { name, power, defense } = fighterData;
  const isNotOddValues = Object.keys(fighterData).every(val => Object.keys(fighter).includes(val));

  if (isName && /\d/.test(name)) {
    response.status(400).json({
      error: 'true',
      message: 'Name can\'t contain numbers'
    });
    return false;
  }
  if (isPower && (power > 100 || !Number.isInteger(+power))) {
    response.status(400).json({
      error: 'true',
      message: 'Power can\'t be more than 100 of string'
    });
    return false;
  }
  if (isDefense && (defense > 10 || !Number.isInteger(+defense))) {
    response.status(400).json({
      error: 'true',
      message: 'Defence can\'t be more than 10 or string'
    });
    return false;
  }
  if (!isNotOddValues) {
    response.status(400).json({
      error: 'true',
      message: 'Error! Odd values in request'
    });
    return false;
  }
  return true;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;