const { user } = require('../models/user');

const createUserValid = (req, res, next) => {

  const { body: { firstName, lastName, email, phoneNumber, password, id } } = req;
  const userData = [firstName, lastName, email, phoneNumber, password];

  const isDataExist = userData.every(value => !!value);

  if (id) {
    res.status(400).json({
      error: 'true',
      message: 'Failed to confirm user data'
    });
  } else if (!isDataExist) {
    res.status(400).json({
      error: 'true',
      message: 'Register data is not complete'
    });
  } else if (isDataValid({ isEmail: true, isPhone: true, isPass: true }, req.body, res)) {
    req.body = { firstName, lastName, email, phoneNumber, password };
    next();
  }
}

const updateUserValid = (req, res, next) => {
  const { body: { firstName, lastName, email, phoneNumber, password } } = req;

  if (Object.keys(req.body).length === 0) {
    res.status(400).json({
      error: 'true',
      message: 'Nothing to update'
    });
  } else if (isDataValid({ isEmail: !!email, isPhone: !!phoneNumber, isPass: !!password }, req.body, res)) {
    req.body = { firstName, lastName, email, phoneNumber, password };
    Object.keys(req.body).forEach(key => !req.body[key] && delete req.body[key]);
    next();
  }
}

function isDataValid({ isEmail = false, isPhone = false, isPass = false }, userData, response) {
  const { email, password, phoneNumber } = userData;
  const phoneRegex = /^(\+380)\d{9}/gi;
  const emailRegex = /^[a-zA-Z1-9]{1}(\w|.){5,29}@{1}gmail.com$/gi;
  const isNotOddValues = Object.keys(userData).every(val => Object.keys(user).includes(val));


  if (isEmail && !emailRegex.test(email)) {
    response.status(400).json({
      error: 'true',
      message: `Email - ${email} isn't valid.\nPlease use valid gmail service email`
    });
    return false;
  }

  if (isPhone && (!phoneRegex.test(phoneNumber) || phoneNumber.length !== 13)) {
    response.status(400).json({
      error: 'true',
      message: `P. Number - ${phoneNumber} isn't valid.\nPlease use valid p. number starts with +380xxxxxxxxx`
    });
    return false;
  }

  if (isPass && password.length < 3) {
    response.status(400).json({
      error: 'true',
      message: 'Password is too short. It must contain at least 3 characters.'
    });
    return false;
  }

  if (!isNotOddValues) {
    response.status(400).json({
      error: 'true',
      message: 'Error! Odd values in request'
    });
    return false;
  }

  return true;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;